from lib.xlsx import Workbook
import re

workbook = Workbook('selection.xlsx')
rows = workbook['_ITU'].rows().iteritems()
rows = map(lambda r: (r[1][2].value, r[1][3].value), rows)

for r in rows:
	initials = ''.join(map(lambda w: w[0], re.sub(r'[^\w\s]', '', r[1]).split()))
	code = re.sub(r'\W', '_', r[0])
	vName = ("%s_%s" % (initials, code)).lower()
	print '%s\t\t\t\t\t\t%s' % (vName, r[0])
